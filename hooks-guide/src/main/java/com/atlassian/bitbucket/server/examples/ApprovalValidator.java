package com.atlassian.bitbucket.server.examples;

import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;

import javax.annotation.Nonnull;

public class ApprovalValidator implements SettingsValidator {

    @Override
    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors errors, @Nonnull Scope scope) {
        try {
            if (settings.getInt("approvals", 0) <= 0) {
                errors.addFieldError("approvals", "Number of approvals must be greater than zero");
            }
        } catch (NumberFormatException e) {
            errors.addFieldError("approvals", "Number of approvals must be a number");
        }
    }
}
